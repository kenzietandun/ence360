/* Title: pipe.c
 * Description: ENCE360 Example code - Pipes
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>
#include <ctype.h>

// Creates a pipe from parent to child
// index 1 is used for writing to the child
// index 0 is used for reading from the child
int parent_to_child[2];

// Creates a pipe from child to parent
// index 1 is used for writing to the parent
// index 0 is used for reading from the parent
int child_to_parent[2];

int child_status, size;
static char message[BUFSIZ];

#define INP 1
#define OUTP 0

int main(int argc, char *argv[]) {



    if (argc != 2) {
        fprintf(stderr, "Usage: %s message\n", argv[0]);
        exit(1);
    }

    /* Create pipes */
    if (pipe(parent_to_child) == -1) {
        perror("Pipe from");
        exit(2);
    }
    if (pipe(child_to_parent) == -1) {
        perror("Pipe - to");
        exit(2);
    }

    switch (fork()) { /* Fork a new process */

    case -1: /* Error forking */
        perror("Fork");
        exit(3);

    case 0: /* Child code */
        // Closes the input channel from parent to child
        close(parent_to_child[INP]);
        // Closes the output channel from child to parent
        close(child_to_parent[OUTP]);

        /* Read from parent */
        if (read(parent_to_child[OUTP], message, BUFSIZ) != -1) {
            printf("CHILD: Recieved %s\n", message);
        } else {
            perror("Read");
            exit(4);
        }

        message[0] = toupper((unsigned char) message[0]);
        if (write(child_to_parent[INP], message, strlen(message)) != -1) {
            printf("CHILD: Sent %s\n", message);
        } else {
            perror("Read");
            exit(4);
        }

        // We are done.
        // Closes the output channel from parent to child
        close(parent_to_child[OUTP]);
        // Closes the input channel from child to parent
        close(child_to_parent[INP]);

        break;

    default: /* Parent code */
        close(parent_to_child[OUTP]);
        close(child_to_parent[INP]);

        if (write(parent_to_child[INP], argv[1], strlen(argv[1])) != -1) {
            printf("PARENT: Sent %s\n", argv[1]);
        } else {
            perror("Write");
            exit(5);
        }

        if (read(child_to_parent[OUTP], message, BUFSIZ) != -1) {
            printf("PARENT: Received: %s\n", message);
        } else {
            perror("Write");
            exit(5);
        }

        wait(&child_status);

        /* Close pipes */
        close(parent_to_child[INP]);
        close(child_to_parent[OUTP]);
    }

    return EXIT_SUCCESS;
}
