/* Title: pipedup2.c
 * Description: ENCE360 Example code - dup2.
 */

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define INP 1
#define OUTP 0

int main(void) {
    /* int fd[2]; */
    /* pid_t childpid; */

    /* pipe(fd); */
    /* if ((childpid = fork()) == 0) { /1* Child code: Runs ls *1/ */
    /*     dup2(fd[INP], STDOUT_FILENO); */
    /*     close(fd[OUTP]); */
    /*     close(fd[INP]); */
    /*     execl("/bin/ls", "ls", "-l", NULL); */
    /*     perror("The exec of ls failed"); */
    /* } */

    /* else { /1* Parent code: Runs sort *1/ */
    /*     dup2(fd[OUTP], STDIN_FILENO); */
    /*     close(fd[OUTP]); */
    /*     close(fd[INP]); */
    /*     execl("/usr/bin/sort", "sort", "-k", "+8", NULL); */
    /*     /1* Note: The location of sort depends on your distribution. */
    /*      * Use 'which sort' to find the correct location *1/ */
    /*     perror("The exec of sort failed"); */
    /* } */

    /* exit(0); */

    int pid;
    int pipe1[2];
    int pipe2[2];
    // create pipe 1
    if (pipe(pipe1) == -1) {
        perror("bad pipe1");
        exit(1);
    }

    // fork (ls -l)
    if ((pid = fork()) == -1) {
        perror("bad fork1");
        exit(1);
    } else if (pid == 0) {
        dup2(pipe1[INP], STDOUT_FILENO);

        // close file descriptors
        close(pipe1[INP]);
        close(pipe1[OUTP]);

        execl("/usr/bin/ls", "ls", "-l", NULL);
        perror("failed ls");
        exit(1);
    }

    if (pipe(pipe2) == -1) {
        perror("bad pipe2");
        exit(1);
    }

    // sort
    if ((pid = fork()) == -1) {
        perror("bad fork2");
        exit(1);
    } else if (pid == 0) {
        dup2(pipe1[OUTP], STDIN_FILENO);
        dup2(pipe2[INP], STDOUT_FILENO);
        close(pipe1[OUTP]);
        close(pipe1[INP]);
        close(pipe2[OUTP]);
        close(pipe2[INP]);

        execl("/usr/bin/sort", "sort", "-k", "+9", NULL);
        perror("failed sort");
        exit(1);
    }

    close(pipe1[OUTP]);
    close(pipe1[INP]);

    // head
    if ((pid = fork()) == -1) {
        perror("bad fork3");
        exit(1);
    } else if (pid == 0) {
        dup2(pipe2[OUTP], STDIN_FILENO);
        close(pipe2[OUTP]);
        close(pipe2[INP]);

        execl("/usr/bin/head", "head", "-5", NULL);
        perror("failed head");
        exit(1);
    }
    exit(0);
}
