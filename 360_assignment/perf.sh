#!/bin/bash
set -v

make

time ./bin/downloader large.txt 1 download && rm download/*
time ./bin/downloader large.txt 2 download && rm download/*
time ./bin/downloader large.txt 4 download && rm download/*
time ./bin/downloader large.txt 8 download && rm download/*
time ./bin/downloader large.txt 16 download && rm download/*

time ./bin/downloader small.txt 1 download && rm download/*
time ./bin/downloader small.txt 2 download && rm download/*
time ./bin/downloader small.txt 4 download && rm download/*
time ./bin/downloader small.txt 8 download && rm download/*
time ./bin/downloader small.txt 16 download && rm download/*

#time ./bin/downloader 1mb.txt 8 download && rm download/*
#time ./bin/downloader 4.4mb.txt 8 download && rm download/*
#time ./bin/downloader 15mb.txt 8 download && rm download/*
#time ./bin/downloader 46.7mb.txt 8 download && rm download/*
#time ./bin/downloader 83mb.txt 8 download && rm download/*
#time ./bin/downloader 107mb.txt 8 download && rm download/*
