#include <arpa/inet.h>
#include <assert.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/socket.h>
#include <unistd.h>

#include "http.h"

#define MAX_SIZE 10 * 1024 * 1024
#define BUF_SIZE 1024

/**
 * Creates a buffer with default size of BUFSIZ
 * and returns a pointer to it
 *
 * @return Buffer - Pointer to a buffer holding response data from query
 *                  NULL is returned on failure.
 */
Buffer *create_buffer() {
    Buffer *buffer = (Buffer *)malloc(sizeof(Buffer));
    buffer->data = calloc(BUF_SIZE, sizeof(char));
    memset(buffer->data, '\0', BUF_SIZE);
    buffer->length = BUF_SIZE;
    return buffer;
}

/**
 * Creates a TCP socket that will handle communication
 * with a remote web server
 *
 * @return socket - file descriptor of the socket
 */
int create_client_socket() {
    int client_socket = socket(AF_INET, SOCK_STREAM, 0);
    return client_socket;
}

/**
 * Creates a connection to a remote webserver using a
 * socket specified
 */
void connect_client_socket(int fd, char *host, char *port) {
    struct addrinfo *server_address = NULL;
    struct addrinfo hints;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    getaddrinfo(host, port, &hints, &server_address);
    connect(fd, server_address->ai_addr, server_address->ai_addrlen);
    freeaddrinfo(server_address);
}

int send_get_request(int fd, char *host, char *page, const char *range) {
    int max_request_size = 2000;
    char request[max_request_size];
    snprintf(request, max_request_size,
             "GET /%s HTTP/1.0\r\n"
             "Host: %s\r\n"
             "Range: bytes=%s\r\n"
             "User-Agent: getter\r\n\r\n",
             page, host, range);
    return write(fd, request, strlen(request));
}

int send_head_request(int fd, char *host, char *page) {
    int max_request_size = 1000;
    char request[max_request_size];
    snprintf(request, max_request_size,
             "HEAD /%s HTTP/1.0\r\n"
             "Host: %s\r\n"
             "User-Agent: getter\r\n\r\n",
             page, host);
    return write(fd, request, strlen(request));
}

/**
 * Reads the response from the request and saves the data
 * to a buffer specified
 *
 * @param fd - file descriptor pointing to the socket connected to the webserver
 * @param buffer - buffer where we are storing the response data
 */
void read_response(int fd, Buffer *buffer) {
    int read_length = 0;
    int read_total = 0;
    Buffer *temp = create_buffer();
    while ((read_length = read(fd, temp->data, BUF_SIZE)) > 0) {
        // Resize the buffer that stores all the html contents if needed
        if (read_length + read_total >= buffer->length) {
            char *temp = realloc(buffer->data, read_length + read_total);
            if (temp != NULL) {
                buffer->data = temp;
                buffer->length = read_total + read_length;
            }
        }

        memcpy(buffer->data + read_total, temp->data, read_length);
        read_total += read_length;

        // Reset temp buffer for next reading
        memset(temp->data, '\0', BUF_SIZE);
    }
    buffer_free(temp);
    buffer->length = read_total;
}

/**
 * Perform an HTTP 1.0 query to a given host and page and port number.
 * host is a hostname and page is a path on the remote server. The query
 * will attempt to retrieve content in the given byte range.
 * User is responsible for freeing the memory.
 *
 * @param host - the host name e.g. www.canterbury.ac.nz
 * @param page - e.g. /index.html
 * @param range - Byte range e.g. 0-500. NOTE: A server may not respect this
 * @param port - e.g. 80
 * @return Buffer - Pointer to a buffer holding response data from query
 *                  NULL is returned on failure.
 */
Buffer *http_query(char *host, char *page, const char *range, int port) {
    Buffer *buffer = create_buffer();
    int client_socket = create_client_socket();
    char port_str[8];
    sprintf(port_str, "%d", port);
    connect_client_socket(client_socket, host, port_str);
    send_get_request(client_socket, host, page, range);
    read_response(client_socket, buffer);
    close(client_socket);

    return buffer;
}

/**
 * Separate the content from the header of an http request.
 * NOTE: returned string is an offset into the response, so
 * should not be freed by the user. Do not copy the data.
 * @param response - Buffer containing the HTTP response to separate
 *                   content from
 * @return string response or NULL on failure (buffer is not HTTP response)
 */
char *http_get_content(Buffer *response) {
    char *header_end = strstr(response->data, "\r\n\r\n");

    if (header_end) {
        return header_end + 4;
    } else {
        return response->data;
    }
}

/**
 * Splits an HTTP url into host, page. On success, calls http_query
 * to execute the query against the url.
 * @param url - Webpage url e.g. learn.canterbury.ac.nz/profile
 * @param range - The desired byte range of data to retrieve from the page
 * @return Buffer pointer holding raw string data or NULL on failure
 */
Buffer *http_url(const char *url, const char *range) {
    char host[BUF_SIZE];
    strncpy(host, url, BUF_SIZE);

    char *page = strstr(host, "/");

    if (page) {
        page[0] = '\0';

        ++page;
        return http_query(host, page, range, 80);
    } else {

        fprintf(stderr, "could not split url into host/page %s\n", url);
        return NULL;
    }
}

char *split_domain_path(char *url) {
    char *split_pos = strstr(url, "/");
    *split_pos = '\0';
    return split_pos + 1;
}

int http_get_content_length(Buffer *buffer) {
    char *content_length = "Content-Length: ";
    char *length_pos = strstr(buffer->data, content_length);
    length_pos += strlen(content_length);
    char *newline = strstr(length_pos, "\n");
    *newline = '\0';

    return atoi(length_pos);
}

/**
 * Makes a HEAD request to a given URL and gets the content length
 * Then determines max_chunk_size and number of split downloads needed
 * @param url   The URL of the resource to download
 * @param threads   The number of threads to be used for the download
 * @return int  The number of downloads needed satisfying max_chunk_size
 *              to download the resource
 */
int get_num_tasks(char *url, int threads) {
    char *url_copy = strdup(url);

    char *host = url_copy;
    char *page = split_domain_path(url_copy);

    Buffer *buffer = create_buffer();
    int client_socket = create_client_socket();
    char *port_str = "80";

    connect_client_socket(client_socket, host, port_str);
    send_head_request(client_socket, host, page);

    read_response(client_socket, buffer);
    int filesize = http_get_content_length(buffer);
    max_chunk_size = filesize / threads;

    free(url_copy);
    buffer_free(buffer);
    close(client_socket);

    return threads;
}

int get_max_chunk_size() { return max_chunk_size; }
