# ENCE360 Assignment Report

Kenzie Tandun (kta79)

## Algorithm Analysis

From `downloader.c`, Line `200` to `230`:

```C
200   Context *context = spawn_workers(num_workers);
```

`spawn_workers` function is called to create threads that will be responsible
for downloading files that are split into smaller chunks. It is saved into a
`Context` struct which we can use to keep track finished and ongoing downloads.

```C
201
202   int work = 0, bytes = 0, num_tasks = 0;
203   while ((len = getline(&line, &len, fp)) != -1) {
```

The `while` loop goes through each line in the file we specified from the
command line arguments.

```C
204
205      if (line[len - 1] == '\n') {
206          line[len - 1] = '\0';
207      }
```

If the line ends in a newline, it indicates the end of URL. The newline is
replaced with a `NULL` terminator to indicate it is the end of the string.

```C
208
209      num_tasks = get_num_tasks(line, num_workers);
210      bytes = get_max_chunk_size();
```

Using the helper function we implemented beforehand, the number of download
tasks and total bytes we download for each task are saved into their respective
variables.

```C
211
212      for (int i  = 0; i < num_tasks; i ++) {
213          ++work;
214          queue_put(context->todo, new_task(line, i * bytes, (i+1) * bytes));
215      }
```

After we know the number of tasks we have to download a single URL, we put those
tasks into our queue. Here we also incremented the `work` variable to keep track
of ongoing tasks.

```C
216
217      // Get results back
218      while (work > 0) {
219          --work;
220          wait_task(download_dir, context);
221      }
```

This function will block until all of the tasks for downloading a single URL is
done.

```C
222
223      /* Merge the files -- simple synchronous method
224       * Then remove the chunked download files
225       * Beware, this is not an efficient method
226       */
227      merge_files(download_dir, line, bytes, num_tasks);
228      remove_chunk_files(download_dir, bytes, num_tasks);
229  }
```

After all of the chunks are downloaded, the files are merged into a single file
and artifacts generated from the previous methods are removed.

## Performance Analysis

| File      | Threads | Time Taken |
| --------- | ------- | ---------- |
| large.txt | 1       | 1m8.722s   |
| large.txt | 2       | 1m2.793s   |
| large.txt | 4       | 0m43.594s  |
| large.txt | 8       | 0m40.749s  |
| large.txt | 16      | 0m48.190s  |
| small.txt | 1       | 0m0.816s   |
| small.txt | 2       | 0m0.636s   |
| small.txt | 4       | 0m0.569s   |
| small.txt | 8       | 0m0.559s   |
| small.txt | 16      | 0m0.562s   |

The performance test was done using the URLs provided in `large.txt` and
`small.txt`, the program `time` was used to measure the time taken for the
program to finish. The computer is connected to wifi using `UCwireless` for the
whole tests. In both `large.txt` and `small.txt`, the program took the least
time to complete downloading every URL when the thread count was set to 8. This
might be due to some factors such as:

- Limitation by the network speed on our/the client side. The program might have
  used all of the available bandwidth therefore increasing the number of threads
  does not increase its performance.
- Server limitation. The server that served the requests might flag the requests
  as spam when there were too many requests from the same origin, therefore
  reducing the speed.
- As we increase the number of threads, the program splitted the file into
  smaller chunks. Combining many small files took longer time since more
  read/write operations will be needed.

In the second experiment, the number of threads were kept constant at 8 but the
size of the file was increased every time. The URL used was
`opensuse.mirrors.theom.nz`, a Linux mirror website in New Zealand.

| File Size (MB) | Threads | Time Taken |
| -------------- | ------- | ---------- |
| 1              | 8       | 0m 0.251s  |
| 4.4            | 8       | 0m 1.184s  |
| 15             | 8       | 0m 3.466s  |
| 46.7           | 8       | 0m 10.775s |
| 83             | 8       | 0m 20.970s |
| 107            | 8       | 0m 37.759s |

Up until 80MB, the time taken scaled linearly with the size of the file. This
trend stopped continuning with larger file as seen on the last row. There are
some improvements we could do to increase performance, such as modifying how we
combine the files after they are downloaded. Instead of making many small
chunks, we could probably allocate a single buffer that has the size of the file
that we are downloading and each thread would fill the buffer with the contents
they downloaded, removing the need to merge the chunks at the end. The downside
of this method is it would use a large amount of memory if the file size was large.

### Comparison with pre-compiled binary

| File      | Threads | My downloader | Pre-compiled downloader |
| --------- | ------- | ------------- | ----------------------- |
| large.txt | 1       | 1m 8.722s     | 0m 54.925s              |
| large.txt | 2       | 1m 2.793s     | 0m 46.962s              |
| large.txt | 4       | 0m 43.594s    | 0m 42.186s              |
| large.txt | 8       | 0m 40.749s    | 0m 46.686s              |
| large.txt | 16      | 0m 48.190s    | 0m 49.771s              |
| small.txt | 1       | 0m 0.816s     | 0m 0.693s               |
| small.txt | 2       | 0m 0.636s     | 0m 0.610s               |
| small.txt | 4       | 0m 0.569s     | 0m 0.579s               |
| small.txt | 8       | 0m 0.559s     | 0m 0.539s               |
| small.txt | 16      | 0m 0.562s     | 0m 0.552s               |

There seemed to be minimal difference between our and the pre-compiled
downloader. This might be caused by the fluctuating network connection since the
pre-compiled binary were faster some cases and slower in others.
