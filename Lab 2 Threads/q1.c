#include <assert.h>
#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int has_run[10] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

void runMe(int *arg) {
    int value = (*arg);
    assert(value >= 0 && value < 5 && "Bad argument passed to 'runMe()!'");

    has_run[value] = 1;

    int *ret = (int *)malloc(sizeof(int));
    *ret = value * value;

    pthread_exit((void *)ret);
}

int run_threads(int n) {
    int sum[n];
    int total = 0;
    void* temp;
    pthread_t threads[n];

    for (int i = 0; i < n; i++) {
        sum[i] = i;
        pthread_create(&threads[i], NULL, (void *) runMe, &sum[i]);
    }

    for (int i = 0; i < n; i++) {
        pthread_join(threads[i], &temp);
        total += *(int *) temp;
    }

    return (int) total;
}

int main(int argc, char **argv) {

    int sum = run_threads(5);

    int correct = 0;
    for (int i = 0; i < 5; ++i) {
        if (has_run[i])
            correct++;
    }

    printf("%d %d", correct, sum);

    return 0;
}
